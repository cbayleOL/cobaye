#! /bin/bash
TOKEN="$1"
# Google.com
#JWKSURI="https://www.googleapis.com/oauth2/v3/certs"
# Gitlab.com
JWKSURI="https://gitlab.com/-/jwks"

pad_b64() {
	case $(( ${#1} % 4 )) in
        	2) echo -n "${1}=="   ;;
        	3) echo -n "${1}="    ;;
        	*) echo -n "${1}"     ;;
	esac
}

urldecode(){
	tr -- '-_' '+/'
}

en2pubkey() {
    local e="$1"
    local n="$2"
    echo "
        asn1 = SEQUENCE:pubkeyinfo
        [pubkeyinfo]
        algorithm = SEQUENCE:rsa_alg
        pubkey = BITWRAP,SEQUENCE:rsapubkey
        [rsa_alg]
        algorithm = OID:rsaEncryption
        parameter = NULL
        [rsapubkey]
        n = INTEGER:0x$n
        e = INTEGER:0x$e
    " | sed '/^$/d ; s/^ *//g'              \
    | openssl asn1parse                     \
        -genconf    /dev/stdin              \
        -out        /dev/stdout             \
    | openssl rsa                           \
        -pubin                              \
            -inform     DER                 \
            -outform    PEM                 \
            -in         /dev/stdin          \
            -out        $3
}

extractpubkey(){
	local jwksuri="$1"
	local kid="$2"
	local pubkeyfile=$3
	jwksjson=$(curl -s $jwksuri)
	echo $jwksjson | jq ".keys[] | select(.kid | contains(\"$kid\")) "

	n=$(echo $jwksjson | jq -r ".keys[] | select(.kid | contains(\"$kid\")) | .n")
	e=$(echo $jwksjson | jq -r ".keys[] | select(.kid | contains(\"$kid\")) | .e")

	hexa_n=$(pad_b64 $n | urldecode | base64 -d | xxd -p -u | tr -d '\n')
	hexa_e=$(pad_b64 $e | urldecode | base64 -d | xxd -p -u | tr -d '\n')

	en2pubkey $hexa_e $hexa_n $pubkeyfile
}

# Split token
header=$(echo -n $TOKEN | cut -d "." -f1)
payload=$(echo -n $TOKEN | cut -d "." -f2)
sig=$(echo -n $TOKEN | cut -d "." -f3)

# Extract token infos
h=$(pad_b64 $header | urldecode | base64 -d)
p=$(pad_b64 $payload | urldecode | base64 -d)
s=$(pad_b64 $sig | urldecode | base64 -d)

alg=$(echo $h | jq -r .alg)
kid=$(echo $h | jq -r .kid)

# Display token infos
echo "================== TOKEN INFO =================="
echo $h | jq .
echo $p | jq . | grep -v user_
#echo $s

# Extract pubkey form jwks uri
echo "================== EXTRACT PUBKEY =================="
extractpubkey $JWKSURI $kid pubkey

# Extract signature 
echo -n "$s" > sigfile

# Extract data (header+payload)
echo -n "$header.$payload" > data

# Validate token
echo "================== CHECK SIGNATURE =================="
openssl dgst -sha256 -verify pubkey -signature sigfile data
